#!/bin/bash

tf_plan() {
    pushd _workspace/terraform/$1
    if ![[ terraform plan --target="$2" --out=$2.tfplan ]]; then
        echo "Terraform has encountered an error. This is most likely a Displat issue, please report on our Discord (support.displat.net)" && exit 1
    fi
    popd +0
}

tf_apply() {
    echo "Applying infrastructure changes. Please be patient, this process can take some time."
    pushd $1
    if ![[ terraform apply $2.tfplan -auto-approve ]]; then
        echo "Terraform has encountered an error. This is most likely a Displat issue, please report on our Discord (support.displat.net)" && exit 1
    fi
    popd +0
}
